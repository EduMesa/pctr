
/**
 * Esta clase hace uso de la base de datos
 * de la DGT creada en la clase Conductores
 * a traves de hilos que modifican datos
 * de los conductores de dicha base de datos
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

 import java.util.Random;
import java.util.Scanner;

public class usaConductores implements Runnable {

    //Tipo de hilo para adjudicarle un parametro a modificar
    private int tipoHilo;

    //Array de conductores pertenecientes a la base de datos
    private static Conductor[] conductores;
    
    //Referencia a la base de datos para su uso
    private static Conductores conductorDB;

    //Booleano para inicializar los datos estaticos una sola vez
    private static boolean once = false;

    //Objeto de la clase random para modificar aleatoriamente valores de los conductores
    private Random r;

    /**
     * Constructor de la clase
     * @param t tipo de hilo
     * @param tam tamaño del array de tipo Conductor
     */
    public usaConductores(int t, int tam) {
        tipoHilo = t;
        r = new Random();
        if (!once) {
            conductores = new Conductor[tam];
            conductorDB = new Conductores();
            for (int i = 0; i < conductores.length; i++) {
                conductores[i] = new Conductor("Conductor" + i, String.valueOf(600000000 + r.nextInt(99999999)),
                        18 + r.nextInt(100), "b1");
            }
            once = true;
        }
    }

    /**
     * Metodo observador que devuelve el array 
     * de conductores en la base de datos
     * @return
     */
    public static Conductor[] GetConductores() {
        return conductores;
    }

    /**
     * Sobrecarga del metodo run
     */
    public void run() {
        switch (tipoHilo) {
        case 0:
            conductorDB.SetEdad(conductores[r.nextInt((conductores.length))], (18 + r.nextInt(100)));
            break;
        case 1:
            conductorDB.SetNombre(conductores[r.nextInt((conductores.length))], "ConductorNuevo");
            break;
        case 2:
            conductorDB.SetTelefono(conductores[r.nextInt((conductores.length))],
                    (String.valueOf(600000000 + r.nextInt(99999999))));
            break;
        case 3:
            conductorDB.SetTipoCarnet(conductores[r.nextInt((conductores.length))], "a1");
            break;
        }
    }

    /**
     * Metodo main
     * @param args
     */
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Introduzca numero de hebras que van a usar la base de datos: ");
        Thread hilos[] = new Thread[s.nextInt()];
        Random rMain = new Random();
        System.out.print("Introduzca numero de conductores almacenados en la base de datos: ");
        int nCon = s.nextInt();

        for (int i = 0; i < hilos.length; i++) {
            hilos[i] = new Thread(new usaConductores(rMain.nextInt(4), nCon));
            hilos[i].start();
        }

        for (int i = 0; i < hilos.length; i++) {
            try {
                hilos[i].join();
            } catch (InterruptedException ex) {
            }
        }

        for (Conductor cnd : GetConductores()) {
            System.out.println(
                    "Nombre: " + conductorDB.GetNombre(cnd) + ", edad: " + conductorDB.GetEdad(cnd) + ", telefono: "
                            + conductorDB.GetTelefono(cnd) + ", tipo carnet: " + conductorDB.GetTipoCarnet(cnd));
        }
    }
}