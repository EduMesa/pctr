
/**
 * Esta clase modela la base de datos
 * de la DGT que hace cambios en los conductores
 * que hay en ella
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

 public class Conductores
{
    /**
     * Metodo observador del nombre
     * @param c Objeto clase Conductor
     * @return Nombre del conductor
     */
    public String GetNombre(Conductor c)
    {
        return c.GetNombre();
    }

    /**
     * Metodo observador del telefono
     * @param c Objeto clase Conductor
     * @return Telefono del conductor
     */
    public String GetTelefono(Conductor c)
    {
        return c.GetTelefono();
    }

    /**
     * Metodo observador de la edad
     * @param c Objeto clase Conductor
     * @return Edad del conductor
     */
    public int GetEdad(Conductor c)
    {
        return c.GetEdad();
    }

    /**
     * Metodo observador del tipo de carnet
     * @param c Objeto clase Conductor
     * @return Tipo de carnet del conductor
     */
    public String GetTipoCarnet(Conductor c)
    {
        return c.GetTipoCarnet();
    }

    /**
     * Metodo modificador del nombre
     * @param c Objeto clase Conductor
     * @param nombre Nuevo nombre del conductor
     */
    public void SetNombre(Conductor c, String nombre)
    {
        c.SetNombre(nombre);
    }

    /**
     * Metodo modificador del telefono
     * @param c Objeto clase Conductor
     * @param telefono Nuevo telefono del conductor
     */
    public void SetTelefono(Conductor c, String telefono)
    {
        c.SetTelefono(telefono);
    }

    /**
     * Metodo modificador de la edad
     * @param c Objeto clase Conductor
     * @param edad Nueva edad del conductor
     */
    public void SetEdad(Conductor c, int edad)
    {
        c.SetEdad(edad);
    }
    
    /**
     * Metodo modificador del tipo de carnet
     * @param c Objeto clase Conductor
     * @param tipoCarnet Nuevo tipo de carnet del conductor
     */
    public void SetTipoCarnet(Conductor c, String tipoCarnet)
    {
        c.SetTipoCarnet(tipoCarnet);
    }
}