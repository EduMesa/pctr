
/**
 * Esta clase hace uso del monitor lectorEscritor
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

 public class usalectorEscritor implements Runnable {
 
    // Objeto monitor comun
    private lectorEscritor monitor;

    // Booleano para indicar si es escritor o lector (true = escritor)
    private boolean escritor;

    /**
     * Constructor de la clase
     * @param monitor objeto comun
     * @param escritor booleano para indicar si es escritor o no
     */
    public usalectorEscritor(lectorEscritor monitor, boolean escritor) {
        this.monitor = monitor;
        this.escritor = escritor;
    }

    /**
     * Sobrecarga metodo run
     */
    public void run() {
        if (escritor) {
            for (int i = 0; i < 100; i++) {

                monitor.inicia_escritura();
                System.out.println("Escribiendo...");
                monitor.fin_escritura();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                }
            }
        } else {
            for (int i = 0; i < 100; i++) {

                monitor.inicia_lectura();
                System.out.println("Leyendo...");
                monitor.fin_lectura();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                }
            }
        }
    }

    /**
     * Metodo main
     */
    public static void main(String[] args) {
        lectorEscritor l = new lectorEscritor(0, false);

        usalectorEscritor r1 = new usalectorEscritor(l, true);
        usalectorEscritor r2 = new usalectorEscritor(l, false);

        Thread h1 = new Thread(r1);
        Thread h2 = new Thread(r2);

        h1.start();
        h2.start();
        try {
            h1.join();
            h2.join();
        } catch (InterruptedException ex) {
        }
    }
}