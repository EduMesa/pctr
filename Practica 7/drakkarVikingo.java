
/**
 * Esta clase modela el problema de los vikingos
 * a traves de un monitor
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

public class drakkarVikingo {
    
    //Recurso compartido
    private int marmita;

    /**
     * Constructor basico
     * @param tam numero de anguilas en la marmita
     */
    public drakkarVikingo(int tam) {
        marmita = tam;
    }

    /**
     * Metodo sincronizado para comer en exclusion mutua.
     * Ademas dentro paramos a los procesos cuando no haya
     * raciones para comer
     */
    public synchronized void comer() {
        while (marmita == 0) {
            notifyAll();
            try {
                wait();
            } catch (InterruptedException ex) {}
        }

        marmita--;
    }

    /**
     * Metodo sincronizado para llenar la marmita en exclusion mutua
     * Ademas comprobamos que la marmita se encuentre vacia para hacerlo
     */
    public synchronized void cocinar() {
        while (marmita > 0) {
            try {
                wait();
            } catch (InterruptedException ex) {}
        }

        marmita = 100;
        notifyAll();
    }
}