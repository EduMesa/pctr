
/**
 * Esta clase hace uso del monitor para
 * el problema de los vikingos
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

public class UsaDrakkarVikingo extends Thread
{
    //Objeto del monitor comun
    public drakkarVikingo monitorComun;
    
    //Booleano para indicar tipo de vikingo
    public boolean tipoVikingo;

    /**
     * Constructor de la clase
     * @param monitor objeto comun
     * @param tipoVikingo tipo de vikingo
     */
    public UsaDrakkarVikingo(drakkarVikingo monitor, boolean tipoVikingo)
    {
        monitorComun = monitor;
        this.tipoVikingo = tipoVikingo;
    }

    /**
     * Sobrecarga del metodo run
     */
    public void run()
    {
        if(tipoVikingo)
        {
            for(int i = 0; i < 100; i++)
            {
                monitorComun.comer();
                System.out.println("Comiendo");
                try{
                    Thread.sleep(100);
                }catch(InterruptedException ex){}
            }
        }else
        {
            for(int i = 0; i < 100; i++)
            {
                monitorComun.cocinar();
                System.out.println("Llenando");
                try{
                    Thread.sleep(100);
                }catch(InterruptedException ex){}
            }
        }
    }

    /**
     * Metodo main
     * @param args
     */
    public static void main(String[] args)
    {
        drakkarVikingo monitor = new drakkarVikingo(100);

        
        UsaDrakkarVikingo vikA = new UsaDrakkarVikingo(monitor, true);
        UsaDrakkarVikingo vikB = new UsaDrakkarVikingo(monitor, false);

        vikA.start();
        vikB.start(); 
        

        try{
            vikA.join();
            vikB.join();
        }catch(InterruptedException ex){}
    }
}