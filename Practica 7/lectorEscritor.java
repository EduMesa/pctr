
/**
 * Esta clase modela el problema lector/escritor
 * a traves de un monitor
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

public class lectorEscritor
{
    
    //Variable que nos indica el numero de lectores leyendo
    private int n;

    //Variable que nos indica si hay un escritor escribiendo
    private boolean escribiendo;

    /**
     * Constructor de la clase
     * @param n numero de lectores leyendo
     * @param escribiendo indica si hay un escritor escribiendo
     */
    public lectorEscritor (int n, boolean escribiendo)
    {
        this.n = n;
        this.escribiendo = escribiendo;
    }

    /**
     * Metodo sincronizado para leer en exclusion mutua
     * Ademas comprobaremos que no se esta escribiendo para
     * permitir leer al proceso
     */
    public synchronized void inicia_lectura()
    {
        while(escribiendo)
        {
            try{
                wait();
            }catch(InterruptedException ex){}
        }
        n++;
        notifyAll();
    }

    /**
     * Metodo sincronizado para terminar la lectura
     * Si el numero de lectores es igual a cero despertaremos
     * a todos los procesos bloqueados por si hubiera un escritor
     * que quisiera escribir
     */
    public synchronized void fin_lectura()
    {
        n--;
        if(n == 0)
        {
            notifyAll();
        }
    }

    /**
     * Metodo sincronizado para escribir en exclusion mutua
     * siempre y cuando no haya algun lector leyendo ni haya 
     * otro escritor escribiendo
     */
    public synchronized void inicia_escritura()
    {
        while( n != 0 || escribiendo)
        {
            try{
                wait();
            }catch(InterruptedException ex){}
        }

        escribiendo = true;
    }

    /**
     * Metodo sincronizado para terminar la escritura
     * una vez finalice despertaremos a los procesos
     * para que lean o escriban
     */
    public synchronized void fin_escritura()
    {
        escribiendo = false;
        notifyAll();
    }
}