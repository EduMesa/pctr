
/**
 * Esta clase modela los objetos conductores de
 * la base de datos. Como suponemos que no se pueden
 * eliminar ni incluir datos en la base de datos solo
 * se modificaran los objetos que contenga por lo que
 * los metodos de esta clase son todos sincronizados
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

 public class Conductor
{
    //Nombre del conductor
    private String nombre;

    //Telefono del conductor
    private String telefono;

    //Edad del conductor
    private int edad;

    //Tipo de carnet del conductor
    private String tipoCarnet;

    /**
     * Constructor de la clase
     * @param nombre Nombre del conductor
     * @param telefono Telefono del conductor
     * @param edad Edad del conductor
     * @param tipoCarnet tipo de carnet del conductor
     */
    public Conductor(String nombre, String telefono, int edad, String tipoCarnet)
    {
        this.nombre = nombre;
        this.telefono = telefono;
        this.edad = edad;
        this.tipoCarnet = tipoCarnet;
    }

    /**
     * Metodo observador para obtener el nombre del conductor
     * @return Nombre del conductor
     */
    public synchronized String GetNombre()
    {
        return nombre;
    }

    /**
     * Metodo observador para obtener el telefono del conductor
     * @return Telefono del conductor
     */
    public synchronized String GetTelefono()
    {
        return telefono;
    }

    /**
     * Metodo observador para obtener la edad del conductor
     * @return Edad del conductor
     */
    public synchronized int GetEdad()
    {
        return edad;
    }

    /**
     * Metodo observador para obtener el tipo de carnet del conductor
     * @return Tipo de carnet del conductor
     */
    public synchronized String GetTipoCarnet()
    {
        return tipoCarnet;
    }

    /**
     * Metodo modificador del nombre del conductor
     * @param nombre Nuevo nombre del conductor
     */
    public synchronized void SetNombre(String nombre)
    {
        this.nombre = nombre;
    }
  
    /**
     * Metodo modificador del telefono del conductor
     * @param telefono Nuevo telefono del conductor
     */
    public synchronized void SetTelefono(String telefono)
    {
        this.telefono = telefono;
    }
  
    /**
     * Metodo modificador de la edad del conductor
     * @param telefono Nueva edad del conductor
     */
    public synchronized void SetEdad(int edad)
    {
        this.edad = edad;
    }
  
    /**
     * Metodo modificador del tipo de carnet del conductor
     * @param telefono Nuevo tipo de carnet del conductor
     */
    public synchronized void SetTipoCarnet(String tipoCarnet)
    {
        this.tipoCarnet = tipoCarnet;
    }
}