
/**
 * Esta clase hace uso del monitor que modela
 * el sistema de impresoras
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class UsamonitorImpresion implements Runnable {
    // Referencia al monitor que compartiran los hilos
    private monitorImpresion refComun;

    /**
     * Constructor basico de la clase
     * 
     * @param r Objeto monitorImpresion
     */
    public UsamonitorImpresion(monitorImpresion r) {
        refComun = r;
    }

    /**
     * Sobrecarga del metodo run
     */
    public void run() {
        for (int i = 0; i < 10; i++) {
            int n = refComun.pedir_impresora();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
            }
            refComun.soltar_impresora(n);
        }
    }

    /**
     * Sobrecarga metodo main
     */
    public static void main(String[] args) {
        ExecutorService pool = Executors.newCachedThreadPool();

        monitorImpresion monitor = new monitorImpresion();

        for (int i = 0; i < 5; i++) {
            pool.submit(new UsamonitorImpresion(monitor));
        }

        pool.shutdown();

        try {
            pool.awaitTermination(1L, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {
        }
    }
}