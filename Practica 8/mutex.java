
/**
 * Esta clase hace uso del monitor que emula el comportamiento del semaforo
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

public class mutex extends Thread {

    // Recurso compartido por los hilos
    private static int n = 0;

    // Objeto del monitor que sera compartido por los hilos
    private monitorSemaforo semaforo;

    /**
     * Constructor de la clase
     * 
     * @param s Objeto de la clase monitorSemaforo
     */
    public mutex(monitorSemaforo s) {
        semaforo = s;
    }

    /**
     * Sobrecarga del metodo run
     */
    public void run() {
        for (int i = 0; i < 10000; i++) {
            semaforo.waitS();
            n++;
            semaforo.signalS();
        }
    }

    /**
     * Metodo main
     */
    public static void main(String[] args) {
        monitorSemaforo s = new monitorSemaforo(1);
        mutex[] hilos = new mutex[20];

        for (int i = 0; i < hilos.length; i++) {
            hilos[i] = new mutex(s);
            hilos[i].start();
        }

        for (int i = 0; i < hilos.length; i++) {
            try {
                hilos[i].join();
            } catch (InterruptedException ex) {
            }
        }

        System.out.println(n);
    }
}