
/**
 * Esta clase modela un monitor para un sistema de tres impresoras
 * 
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

public class monitorImpresion {
    // Array que representa el estado de las impresoras, true = libre
    private boolean[] impresoras;

    // Contador de impresoras libres
    private int impLibres;

    /**
     * Constructor de la clase
     */
    public monitorImpresion() {
        impresoras = new boolean[3];
        for (int i = 0; i < impresoras.length; i++) {
            impresoras[i] = true;
        }
        impLibres = 3;
    }

    /**
     * Este metodo devuelve el indice del array de impresoras que este libre. Si no
     * hay ninguna bloquea al proceso a la espera de que lo haya
     * 
     * @return int indice libre del array
     */
    public synchronized int pedir_impresora() {
        while (impLibres == 0) {
            try {
                wait();
            } catch (InterruptedException ex) {
            }
        }

        int indiceLibre = -1;
        for (int i = 0; i < impresoras.length; i++) {
            if (impresoras[i]) {
                indiceLibre = i;
            }
        }
        impresoras[indiceLibre] = false;
        impLibres--;
        return indiceLibre;
    }

    /**
     * Este metodo libera una impresora del array
     * 
     * @param n indice a liberar
     */
    public synchronized void soltar_impresora(int n) {
        impresoras[n] = true;
        impLibres++;
        notifyAll();
    }
}