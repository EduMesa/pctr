
/**
 * La clase UsamonitorCadena hace uso de los
 * tres procesos a traves de un pool de threads.
 * Para ello crea dos monitores de la clase
 * monitorCadena_1 cuya referencia será compartida
 * por todos los hilos de la clase procesoA y por
 * todos los hilos de la clase procesoB
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class UsamonitorCadena {

    /**
     * Metodo main
     */
    public static void main(String[] args) {
        ExecutorService pool = Executors.newCachedThreadPool();

        monitorCadena_1 monitorA = new monitorCadena_1(100);
        monitorCadena_1 monitorB = new monitorCadena_1(50);

        for (int i = 0; i < 100; i++) {
            pool.submit(new procesoA(monitorA));
        }

        for (int i = 0; i < 50; i++) {
            pool.submit(new procesoB(monitorA, monitorB));
        }

        for (int i = 0; i < 50; i++) {
            pool.submit(new procesoC(monitorB));
        }

        pool.shutdown();

        try {
            pool.awaitTermination(1L, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {
        }
    }
}

/**
 * La clase procesoA genera una matriz que sera insertada en el buffer del
 * monitor
 * 
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

class procesoA implements Runnable {

    // Monitor compartido por los hilos
    private monitorCadena_1 monitorA;

    // Objeto random para rellenar las matrices
    private Random r;

    /**
     * Constructor de la clase
     * 
     * @param m Referencia comun al monitor
     */
    public procesoA(monitorCadena_1 m) {
        r = new Random(System.currentTimeMillis());
        monitorA = m;
    }

    /**
     * Sobrecarga metodo run
     */
    public void run() {
        int[][] matriz = new int[10000][10000];

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                matriz[i][j] = r.nextInt(100) + 1; // Numeros entre 1 y 100
            }
        }

        monitorA.insertar(matriz);
    }
}

/**
 * La clase procesoB obtiene una matriz del buffer del monitor del proceso A y
 * calcula su traspuesta para almacenarla en el buffer del monitor de su proceso
 * 
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

class procesoB implements Runnable {

    // Monitores compartidos por los hilos
    private monitorCadena_1 monitorA, monitorB;

    // Matriz donde se almacena la traspuesta
    private int[][] matriz;

    /**
     * Constructor de la clase
     * 
     * @param mA Referencia comun al monitorA
     * @param mB Referencia comun al monitorB
     */
    public procesoB(monitorCadena_1 mA, monitorCadena_1 mB) {
        monitorA = mA;
        monitorB = mB;
        matriz = new int[10000][10000];
    }

    /**
     * Sobrecarga metodo run
     */
    public void run() {
        int[][] mAux = monitorA.extraer();

        for (int i = 0; i < mAux.length; i++) {
            for (int j = 0; j < mAux[0].length; j++) {
                matriz[i][j] = mAux[j][i];
            }
        }

        monitorB.insertar(matriz);
    }
}

/**
 * La clase procesoC obtiene una matriz del monitor del proceso B y calcula el
 * producto de su diagonal principal
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

class procesoC implements Runnable {

    // Monitor comun compartido por los hilos
    private monitorCadena_1 monitorB;

    // Variable donde se almacena el producto
    private int producto;

    /**
     * Constructor de la clase
     * 
     * @param mB Referencia comun al monitor
     */
    public procesoC(monitorCadena_1 mB) {
        monitorB = mB;
        producto = 1;
    }

    /**
     * Sobrecarga metodo run
     */
    public void run() {
        int[][] mAux = monitorB.extraer();

        for (int i = 0; i < mAux.length; i++) {
            producto *= mAux[i][i];
        }
    }
}
