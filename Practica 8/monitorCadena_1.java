
/**
 * Esta clase modela un monitor para
 * generar elementos en un contenedor
 * de manera sincronizada, teniendo en 
 * cuenta que no se puede incluir elementos
 * en el contenedor si este esta lleno ni
 * se pueden extraer si esta vacio
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

import java.util.ArrayList;
import java.util.Random;

public class monitorCadena_1 {

    // Contenedor donde se almacenan los elementos
    private ArrayList<int[][]> buffer;

    // Numero elementos actualmente
    private int numElementos;

    // Entero que indica en que indice insertar
    private int In_Ptr;

    // Entero que indica en que indice extraer
    private int Out_Ptr;

    // Tamaño del buffer
    private int tamBuffer;

    /**
     * Constructor de la clase
     * @param tamBuffer tamaño del buffer
     */
    public monitorCadena_1(int tamBuffer) {
        buffer = new ArrayList<int[][]>();
        this.tamBuffer = tamBuffer;
        numElementos = In_Ptr = Out_Ptr = 0;
    }

    /**
     * Este metodo inserta una matriz en el buffer
     * 
     * @param matriz matriz a insertar
     */
    public synchronized void insertar(int[][] matriz) {
        while (numElementos == tamBuffer) {
            try {
                wait();
            } catch (InterruptedException ex) {
            }
        }

        buffer.add(In_Ptr, matriz);
        In_Ptr = (In_Ptr + 1) % buffer.size();
        notifyAll();
        numElementos++;
    }

    /**
     * Este metodo extrae una matriz del buffer
     * 
     * @return int[][] matriz que se extrae
     */
    public synchronized int[][] extraer() {
        while (numElementos == 0) {
            try {
                wait();
            } catch (InterruptedException ex) {
            }
        }

        int[][] elemento = buffer.get(Out_Ptr);
        Out_Ptr = (Out_Ptr + 1) % buffer.size();
        numElementos--;
        notifyAll();
        return elemento;
    }
}