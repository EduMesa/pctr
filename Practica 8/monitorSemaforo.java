
/**
 * Esta clase modela un monitor para emular el comportamiento de un semaforo
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

public class monitorSemaforo {

    // Variable entera que tomara valores no negativos
    private int s;

    // Variable para saber si existen procesos bloqueados
    private int procBloqueados;

    /**
     * Constructor de la clase
     * 
     * @param value valor inicial del semaforo
     */
    public monitorSemaforo(int value) {
        s = value;
    }

    /**
     * Este metodo realiza el wait bloqueando un proceso si el valor del semaforo
     * vale 0 y si no decrementando el valor de s
     */
    public synchronized void waitS() {
        if (s == 0) {
            try {
                procBloqueados++;
                wait();
            } catch (InterruptedException ex) {
            }
        } else {
            s--;
        }
    }

    /**
     * Este metodo realiza el signal del semaforo desbloqueando procesos si hay
     * algun proceso bloqueado y si no incrementa la variable s
     */
    public synchronized void signalS() {
        if (procBloqueados > 0) {
            procBloqueados--;
            notify();
        } else {
            s++;
        }
    }
}