import java.util.Random;

public class prodMatrices{
    private int lInf, lSup;
    private final int dimension = 3000;
    private boolean once = false;
    private int[][] matrizA, matrizB, matrizResultado;

    public prodMatrices(int lInf, int lSup) {
        this.lInf = lInf;
        this.lSup = lSup;
        if (!once) {
            matrizA = new int[dimension][dimension];
            matrizB = new int[dimension][dimension];
            matrizResultado = new int[dimension][dimension];

            Random r = new Random();
            for (int i = 0; i < matrizA.length; i++) {
                for (int j = 0; j < matrizA[0].length; j++) {
                    matrizA[i][j] = r.nextInt();
                    matrizB[i][j] = r.nextInt();
                }
            }
            once = true;
        }
    }

    public void CalcularProducto() {
        for (int i = lInf; i < lSup; i++) {
            for (int j = 0; j < matrizB[0].length; j++) {
                for (int k = 0; k < matrizA[0].length; k++) {
                    matrizResultado[i][j] += matrizA[i][k] * matrizB[k][j];
                }
            }
        }
    }

    public static void main(String[] args) {
        int lInf = 0;
        int lSup = 3000;
        
        long iniCronom = System.currentTimeMillis();
        prodMatrices prodMatriz = new prodMatrices(lInf, lSup);
        prodMatriz.CalcularProducto();
        System.out.println("Ha tardado -> " + (((double) System.currentTimeMillis() - iniCronom) / 1000));
    }
}

/**
 * Tamaño matriz: 3k
 * Solución secuencial: 123.65s
 * Solución paralela:
 *  -2 hilos: 
 */