
/**
 * Esta clase calcula la matriz de resaltado
 * de manera secuencial sobre una matriz
 * rellenada aleatoriamente
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

import java.util.Random;
import java.util.Scanner;

public class resImagen {

    //Matriz sobre la que realizar el calculo
    private int[][] matriz;

    //Matriz donde se guardara el resultado
    private double[][] resaltado;

    /**
     * Constructor basico de la clase
     * @param d dimension para las matrices
     */
    public resImagen(int d) {
        matriz = new int[d][d];
        resaltado = new double[d][d];
    }

    /**
     * Metodo para rellenar la matriz inicial
     * con numeros aleatorios entre 0 y 255 
     */
    public void RellenarMatriz() {
        Random r = new Random();

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                matriz[i][j] = r.nextInt(256);
            }
        }
    }

    /**
     * Metodo para calcular el operador de resaltado
     * de cada elemento de la matriz
     */
    public void CalcularResaltado() {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {

                if (i - 1 >= 0 && i + 1 < matriz.length && j - 1 >= 0 && j + 1 < matriz[0].length) {
                    resaltado[i][j] = (4 * matriz[i][j] - matriz[i + 1][j] - matriz[i][j + 1] - matriz[i - 1][j]
                            - matriz[i][j - 1]) / 8;
                } else {
                    if (i - 1 < 0) {
                        if (j - 1 < 0) {
                            resaltado[i][j] = (4 * matriz[i][j] - matriz[i + 1][j] - matriz[i][j + 1] - 0 - 0) / 8;
                        } else if (j + 1 >= matriz[0].length) {
                            resaltado[i][j] = (4 * matriz[i][j] - matriz[i + 1][j] - 0 - 0 - matriz[i][j - 1]) / 8;
                        } else {
                            resaltado[i][j] = (4 * matriz[i][j] - matriz[i + 1][j] - matriz[i][j + 1] - 0
                                    - matriz[i][j - 1]) / 8;
                        }
                    } else if (i + 1 >= matriz.length) {
                        if (j - 1 < 0) {
                            resaltado[i][j] = (4 * matriz[i][j] - 0 - matriz[i][j + 1] - matriz[i - 1][j] - 0) / 8;
                        } else if (j + 1 >= matriz[0].length) {
                            resaltado[i][j] = (4 * matriz[i][j] - 0 - 0 - matriz[i - 1][j] - matriz[i][j - 1]) / 8;
                        } else {
                            resaltado[i][j] = (4 * matriz[i][j] - 0 - matriz[i][j + 1] - matriz[i - 1][j]
                                    - matriz[i][j - 1]) / 8;
                        }
                    } else if (j - 1 < 0) {
                        resaltado[i][j] = (4 * matriz[i][j] - matriz[i + 1][j] - matriz[i][j + 1] - matriz[i - 1][j]
                                - 0) / 8;
                    } else if (j + 1 > matriz[0].length) {
                        resaltado[i][j] = (4 * matriz[i][j] - matriz[i + 1][j] - 0 - matriz[i - 1][j]
                                - matriz[i][j - 1]) / 8;
                    }
                }
            }
        }
    }

    /**
     * Metodo main
     * @param args
     */
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Introduzca dimension de la matriz: ");
        resImagen r = new resImagen(s.nextInt());

        long iniCronom = System.currentTimeMillis();
        r.RellenarMatriz();
        r.CalcularResaltado();
        
        System.out.println("Ha tardado: " + (((double) System.currentTimeMillis() - iniCronom) / 1000) + " segundos");
    }
}