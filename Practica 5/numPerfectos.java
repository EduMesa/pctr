
/**
 * Esta clase encuentra numeros perfectos en un rango dado
 * manera concurrente utilizando la interfaz Callable
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

 import java.util.ArrayList;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.*;

public class numPerfectos implements Callable {
    
    //inicio del subrango de busqueda de cada hilo
    private int ini = 0;

    //final del subrango de busqueda de cada hilo
    private int fin = 0;

    //Objeto donde guardaremos la cantidad de numeros perfectos encontrados
    public Long resultado = new Long(0);

    /**
     * Constructor de la clase
     * @param ini subrango de inicio
     * @param fin subrango de fin
     */
    public numPerfectos(int ini, int fin) {
        this.ini = ini;
        this.fin = fin;
    }

    /**
     * Método que comprueba si un numero dado es perfecto
     * @param n numero a comprobar si es perfecto
     * @return
     */
    public boolean esPerfecto(long n) {
        int suma = 0;
        for (int i = 1; i < n; i++) {
            if (n % i == 0) {
                suma += i;
            }
        }

        if (suma == n) {
            return true;
        }

        return false;
    }

    /**
     * Sobrecarga del metodo call de la interfaz
     */
    public Long call() {
        for (int i = ini; i <= fin; i++) {
            if (esPerfecto(i)) {
                resultado++;
            }
        }

        return resultado;
    }

    /**
     * Metodo main
     * @param args
     */
    public static void main(String[] args) {
        int nProcesos = Runtime.getRuntime().availableProcessors();
        int rango = Integer.parseInt(args[0]);
        int tVentana = rango / nProcesos;
        int ini = 1;
        int fin = tVentana;
        int perfectosTotales = 0;

        ArrayList<Future<Long>> container = new ArrayList<Future<Long>>();

        long iniCronom = System.currentTimeMillis();
        ThreadPoolExecutor pool = new ThreadPoolExecutor(nProcesos, nProcesos, 0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
        for (int i = 0; i < nProcesos; i++) {
            container.add(pool.submit(new numPerfectos(ini, fin)));
            ini = fin + 1;
            fin += tVentana;
        }

        for (Future<Long> future : container) {
            try {
                perfectosTotales += future.get();
            } catch (CancellationException e) {
            } catch (ExecutionException e) {
            } catch (InterruptedException e) {
            }
        }
        long finCronom = System.currentTimeMillis();
        pool.shutdown();

        System.out.println("Perfectos hallados: " + perfectosTotales);
        System.out.println("Tiempo tardado: " + (((double) finCronom - iniCronom) / 1000) + " segundos");

    }
}