
/**
 * Esta clase calcula la matriz de resaltado
 * de manera concurrente sobre una matriz
 * rellenada aleatoriamente
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

import java.util.Random;
import java.util.Scanner;

public class resImagenParalelo extends Thread {

    //limite inferior y superior representan las filas sobre las que realizar los calculos
    private int limiteInferior, limiteSuperior;

    //booleano para inicializar las variables estaticas una sola vez
    private static boolean once = false;

    //matriz inicial
    public static int[][] matriz;

    //matriz final donde se guardaran los resultados
    public static double[][] resaltado;

    /**
     * Constructor basico de la clase
     * @param lInf limite inferior
     * @param lSup limite superior
     * @param dimension dimension de las matrices
     */
    public resImagenParalelo(int lInf, int lSup, int dimension) {
        limiteInferior = lInf;
        limiteSuperior = lSup;
        if (!once) {
            matriz = new int[dimension][dimension];
            resaltado = new double[dimension][dimension];
            Random r = new Random();
            for(int i = 0; i < matriz.length; i++)
            {
                for (int j = 0; j < matriz[0].length; j++)
                {
                    matriz[i][j] = r.nextInt(256);
                }
            }
            once = true;
        }
    }

    /**
     * Sobrecarga del metodo run
     */
    public void run() {
        for (int i = limiteInferior; i < limiteSuperior; i++) {
            for (int j = 0; j < matriz[0].length; j++) {

                if (i - 1 >= 0 && i + 1 < matriz.length && j - 1 >= 0 && j + 1 < matriz[0].length) {
                    resaltado[i][j] = (4 * matriz[i][j] - matriz[i + 1][j] - matriz[i][j + 1] - matriz[i - 1][j]
                            - matriz[i][j - 1]) / 8;
                } else {
                    if (i - 1 < 0) {
                        if (j - 1 < 0) {
                            resaltado[i][j] = (4 * matriz[i][j] - matriz[i + 1][j] - matriz[i][j + 1] - 0 - 0) / 8;
                        } else if (j + 1 >= matriz[0].length) {
                            resaltado[i][j] = (4 * matriz[i][j] - matriz[i + 1][j] - 0 - 0 - matriz[i][j - 1]) / 8;
                        } else {
                            resaltado[i][j] = (4 * matriz[i][j] - matriz[i + 1][j] - matriz[i][j + 1] - 0
                                    - matriz[i][j - 1]) / 8;
                        }
                    } else if (i + 1 >= matriz.length) {
                        if (j - 1 < 0) {
                            resaltado[i][j] = (4 * matriz[i][j] - 0 - matriz[i][j + 1] - matriz[i - 1][j] - 0) / 8;
                        } else if (j + 1 >= matriz[0].length) {
                            resaltado[i][j] = (4 * matriz[i][j] - 0 - 0 - matriz[i - 1][j] - matriz[i][j - 1]) / 8;
                        } else {
                            resaltado[i][j] = (4 * matriz[i][j] - 0 - matriz[i][j + 1] - matriz[i - 1][j]
                                    - matriz[i][j - 1]) / 8;
                        }
                    } else if (j - 1 < 0) {
                        resaltado[i][j] = (4 * matriz[i][j] - matriz[i + 1][j] - matriz[i][j + 1] - matriz[i - 1][j]
                                - 0) / 8;
                    } else if (j + 1 > matriz[0].length) {
                        resaltado[i][j] = (4 * matriz[i][j] - matriz[i + 1][j] - 0 - matriz[i - 1][j]
                                - matriz[i][j - 1]) / 8;
                    }
                }
            }
        }
    }

    /**
     * Metodo main
     * @param args
     */
    public static void main(String[] args) {
        int nProcesos = Runtime.getRuntime().availableProcessors();
        Scanner s = new Scanner(System.in);
        System.out.print("Introduzca la dimension de la matriz: ");
        int dimension = s.nextInt();
        int tVentana = dimension / nProcesos;
        int lInf = 0;
        int lSup = tVentana;
        resImagenParalelo[] hilos = new resImagenParalelo[nProcesos];

        long iniCronom = System.currentTimeMillis();
        for (int i = 0; i < nProcesos - 1; i++) {
            hilos[i] = new resImagenParalelo(lInf, lSup, dimension);
            lInf = lSup;
            lSup += tVentana;
            hilos[i].start();
        }
        
        hilos[nProcesos - 1] = new resImagenParalelo(lInf, dimension, dimension);
        hilos[nProcesos - 1].start();

        for (int i = 0; i < nProcesos; i++) {
            try {
                hilos[i].join();
            } catch (InterruptedException ex) {
            }
        }
        System.out.println("Ha tardado: " + (((double) System.currentTimeMillis() - iniCronom) / 1000) + " segundos");
    }
}