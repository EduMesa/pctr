
/**
 * Esta clase encuentra realiza el producto de dos matrices
 * rellenadas con numeros aleatorios de manera concurrente
 * utilizando para ello la interfaz runnable
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

import java.util.Random;

public class prodMatricesParalelo implements Runnable {

    //lInf y lSup indican las filas a computar por cada hilo
    private int lInf, lSup;

    //Dimension de las matrices
    private final int dimension = 3000;

    //Booleano para inicializar las variables estaticas una sola vez
    private static boolean once = false;

    //Matrices para realizar los calculos.
    private static int[][] matrizA, matrizB, matrizResultado;

    /**
     * Constructor basico de la clase
     * @param lInf inicio del subrango de filas a calcular
     * @param lSup fin del subrango de filas a calcular
     */
    public prodMatricesParalelo(int lInf, int lSup) {
        this.lInf = lInf;
        this.lSup = lSup;
        if (!once) {
            matrizA = new int[dimension][dimension];
            matrizB = new int[dimension][dimension];
            matrizResultado = new int[dimension][dimension];

            Random r = new Random();
            for (int i = 0; i < matrizA.length; i++) {
                for (int j = 0; j < matrizA[0].length; j++) {
                    matrizA[i][j] = r.nextInt();
                    matrizB[i][j] = r.nextInt();
                }
            }
            once = true;
        }
    }

    /**
     * Sobrecarga del metodo run
     */
    public void run() {
        for (int i = lInf; i < lSup; i++) {
            for (int j = 0; j < matrizB[0].length; j++) {
                for (int k = 0; k < matrizA[0].length; k++) {
                    matrizResultado[i][j] += matrizA[i][k] * matrizB[k][j];
                }
            }
        }
    }

    /**
     * Metodo main
     * @param args
     */
    public static void main(String[] args) {
        int nProcesos = Runtime.getRuntime().availableProcessors();
        int tVentana = 3000 / nProcesos;
        int lInf = 0;
        int lSup = tVentana;
        Thread[] hilos = new Thread[nProcesos];
        
        long iniCronom = System.currentTimeMillis();
        for (int i = 0; i < nProcesos - 1; i++) {
            hilos[i] = new Thread(new prodMatricesParalelo(lInf, lSup));
            lInf = lSup;
            lSup += tVentana;
            hilos[i].start();
        }

        hilos[nProcesos - 1] = new Thread(new prodMatricesParalelo(lInf, 3000));
        hilos[nProcesos - 1].start();

        for (int i = 0; i < nProcesos; i++) {
            try {
                hilos[i].join();
            } catch (InterruptedException ex) {
            }
        }

        System.out.println("Ha tardado -> " + (((double) System.currentTimeMillis() - iniCronom) / 1000) + " segundos");
    }
}