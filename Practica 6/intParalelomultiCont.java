
/**
 * Esta clase realiza el metodo de Monte-Carlo
 * de manera paralela, guardando en cada instancia
 * de la clase el numero de puntos obtenidos
 * bajo la curva de la funcion seno
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class intParalelomultiCont implements Runnable {

    //Contador de exitos
    private int cont;

    //Rango a evaluar por cada instancia
    private int lInf, lSup;

    //Variable Random para generar los puntos 
    private Random r;

    /**
     * Constructor de la clase
     * @param lInf limite inferior del subrango
     * @param lSup limite superior del subrango
     */
    public intParalelomultiCont(int lInf, int lSup)
    {
        this.lInf = lInf;
        this.lSup = lSup;
        cont = 0;
        r = new Random(System.currentTimeMillis());
    }

    /**
     * Metodo observador para consultar el numero de aciertos
     * @return
     */
    public int GetCont() {
        return cont;
    }

    /**
     * Sobrecarga del metodo run
     */
    public void run() {
        for (int i = lInf; i < lSup; i++) {
            if (r.nextDouble() < Math.sin(r.nextDouble())) {
                cont++;
            }
        }
    }

    /**
     * Metodo main
     * @param args
     */
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int nProcesos = Runtime.getRuntime().availableProcessors();
        System.out.print("Introduzca numero de puntos: ");
        int nPuntos = s.nextInt();
        int tVentana = nPuntos / nProcesos;
        int lInf = 0;
        int lSup = tVentana;
        intParalelomultiCont[] intP = new intParalelomultiCont[nProcesos];
        
        ThreadPoolExecutor pool = new ThreadPoolExecutor(nProcesos, nProcesos, 1L, TimeUnit.MINUTES,
                new LinkedBlockingQueue<Runnable>());
        
        long iniCronom = System.currentTimeMillis();
        for (int i = 0; i < nProcesos - 1; i++) {
            intP[i] = new intParalelomultiCont(lInf, lSup); 
            pool.submit(intP[i]);
            lInf = lSup;
            lSup += tVentana;
        }

        intP[nProcesos - 1] = new intParalelomultiCont(lInf, nPuntos);
        pool.submit(intP[nProcesos - 1]);
        pool.shutdown();
        try{
            pool.awaitTermination(1L, TimeUnit.MINUTES);
        }catch(InterruptedException ex){}
   
        System.out.println("Ha tardado: " + (((double)System.currentTimeMillis() - iniCronom) / 1000) + " segundos");

        int contAux = 0;
        for(int i = 0; i < nProcesos; i++)
        {
            contAux += intP[i].GetCont();
        }

        System.out.println("Aproximacion: " + ((double) contAux / nPuntos));
    }
}