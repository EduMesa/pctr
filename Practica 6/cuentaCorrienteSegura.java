
/**
 * Esta clase implementa un monitor
 * para acceder en exclusion mutua
 * de la variable compartida "saldo" 
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

public class cuentaCorrienteSegura
{
    //Variable compartida
    private double saldo;

    /**
     * Constructor de la clase
     */
    public cuentaCorrienteSegura(double saldo)
    {
        this.saldo = saldo;
    }

    /**
     * Metodo observador para consultar el recurso compartido
     */
    public synchronized double GetSaldo() { return saldo; }

    /**
     * Este metodo realiza un incremento
     * del recurso compartido en exclusion
     * mutua
     */
    public synchronized void Ingreso()
    {
        saldo++;
    }

    /**
     * Este metodo realiza un decremento
     * del recurso compartido en exclusion
     * mutua
     */
    public synchronized void Reintegro()
    {
        saldo--;
    }
}