/**
 * Esta clase crea un numero de sockets
 * para realizar una conexion con el servidor
 * y cronometra el tiempo que tarda en relaizar
 * sus peticiones
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

 import java.net.*;
import java.io.*;

public class clienteMultiple {

    /**
     * Metodo main
     * @param args
     */
    public static void main(String[] args) {
        int puerto = 2001;
        try {
            long iniCronom = System.currentTimeMillis();
            Socket[] cables = new Socket[3000];
            for (int i = 0; i < cables.length; i++) {
                cables[i] = new Socket("localhost", puerto);
                PrintWriter salida = new PrintWriter(
                        new BufferedWriter(new OutputStreamWriter(cables[i].getOutputStream())));
                salida.println((int) (Math.random() * 10));
                salida.flush();
                cables[i].close();
            }

            System.out.println("Ha tardado -> " + (((double) System.currentTimeMillis() - iniCronom) / 1000) + " segundos");

        } // try
        catch (Exception e) {
            System.out.println("Error en sockets...");
        }
    }// main
}// Cliente_Hilos