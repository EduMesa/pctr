
/**
 * Esta clase crea un fichero de tipo RandomAccesFile
 * y escribe, de manera concurrente, en él 
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class ficheroSeguro extends Thread {
    
    //Referencia compartida por todos los objetos para usarse de cerrojo 
    private static RandomAccessFile fichero;

    /**
     * Constructor de la clase
     */
    public ficheroSeguro() {
        try {
            fichero = new RandomAccessFile("prueba.dat", "rw");
        } catch (FileNotFoundException fx) {
        }
    }

    /**
     * Sobrecarga del metodo run
     */
    public void run()
    {
        synchronized(fichero)
        {
            try{
                fichero.writeChars(""+Math.random()*10+'\n');
            }catch(IOException io){}
       }
    }

    /**
     * Metodo main
     * @param args
     */
    public static void main(String[] args) {

        ficheroSeguro[] threads = new ficheroSeguro[10];

        for (int i = 0; i < threads.length; i++) {
            threads[i] = new ficheroSeguro();
            threads[i].start();
        }

        for (int i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException ex) {
            }
        }
    }
}