
/**
 * Esta clase realiza el metodo de Monte-Carlo
 * de manera paralela, guardando el numero de
 * exitos en una variable compartida controlando
 * su acceso en exclusion mutua 
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

import java.util.Random;
import java.util.Scanner;

public class intParalelaunitCont implements Runnable
{
    //Contador de exitos
    public static int cont;
    
    //Cerrojo compartido por las instancias
    public static Object lock;
    
    //Rango a evaluar por cada instancia
    public int lInf, lSup;
    
    //Variable Random para generar los puntos 
    public Random r;

    /**
     * Constructor vacio de la clase
     */
    public intParalelaunitCont(){}

    /**
     * Constructor de la clase
     * @param lInf limite inferior del subrango
     * @param lSup limite superior del subrango
     */
    public intParalelaunitCont(int lInf, int lSup)
    {
        this.lInf = lInf;
        this.lSup = lSup;
        cont = 0;
        r = new Random(System.currentTimeMillis());
        lock = new Object();
    }

    /**
     * Metodo observador para consultar los exitos
     * @return
     */
    public int GetCont() { return cont; }

    /**
     * Sobrecarga metodo run
     */
    public void run()
    {
        for(int i = lInf; i < lSup; i++)
        {
            if(r.nextDouble() < Math.sin(r.nextDouble()))
            {
                synchronized(lock)
                {
                    cont++;
                }
            }
        }
    }

    /**
     * Metodo main
     * @param args
     */
    public static void main (String[] args)
    {
        Scanner s = new Scanner(System.in);
        int nProcesos = Runtime.getRuntime().availableProcessors();
        System.out.print("Introduzca numero de puntos: ");
        int nPuntos = s.nextInt();
        int tVentana = nPuntos / nProcesos;
        int lInf = 0;
        int lSup = tVentana;
        Thread[] hilos = new Thread[nProcesos];

        long iniCronom = System.currentTimeMillis();
        for(int i = 0; i < nProcesos - 1; i++)
        {
            hilos[i] = new Thread(new intParalelaunitCont(lInf, lSup));
            hilos[i].start();
            lInf = lSup;
            lSup += tVentana;
        }

        hilos[nProcesos - 1] = new Thread(new intParalelaunitCont(lInf, nPuntos));
        hilos[nProcesos - 1].start();

        for(int i = 0; i < nProcesos; i++)
        {
            try{hilos[i].join();}catch(InterruptedException ex){}
        }
        
        System.out.println("Ha tardado: " + (((double)System.currentTimeMillis() - iniCronom) / 1000) + " segundos");
        intParalelaunitCont observer = new intParalelaunitCont();
        System.out.println("Aproximacion: " + ((double)observer.GetCont() / nPuntos));
    }
}