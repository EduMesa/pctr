
/**
 * Esta clase hace uso de la clase
 * cuentaCorrienteSegura a traves de
 * objetos Runnable usando expresiones
 * lambda. 
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class simulaRedCajeros {

    /**
     * Metodo main
     * @param args
     */
    public static void main(String[] args)
    {
        ExecutorService pool = Executors.newCachedThreadPool();
        cuentaCorrienteSegura c = new cuentaCorrienteSegura(0);
        
        Runnable r1 = () ->
        {
            c.Ingreso();
            c.Reintegro();
        };

        System.out.println("Saldo inicial: " + c.GetSaldo());
        for(int i = 1; i <= 100000; i++)
        {
            pool.execute(r1);
        }
        pool.shutdown();
        try{pool.awaitTermination(1L, TimeUnit.MINUTES);}catch(InterruptedException ex){}
        System.out.println("Saldo esperado: 0.0" + '\n' + "Saldo final: " + c.GetSaldo());
    }
}