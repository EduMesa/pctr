
/**
 * Esta clase realiza el producto de una matriz por
 * un vector de manera secuencial rellenados previamente 
 * con numeros al azar
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */
import java.util.Random;

public class matVector {
    
    //Matriz para realizar los cálculos
    private int[][] matrix = new int[10000][10000];

    //Vector para realizar los cálculos
    private int[] vector = new int[10000];

    //Vector donde almacenar el resultado
    private int[] result = new int[10000];

    /**
     * Este método rellena la matriz y el vector
     * de manera aleatoria
     */
    public void SetMatrixAndVector() {
        Random r = new Random();

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = r.nextInt();
            }
            vector[i] = r.nextInt();
        }
    }

    /**
     * Este método realiza los cálculos y los 
     * almacena en el vector resultado.
     */
    public void CalculateProduct() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                result[i] += matrix[i][j] * vector[j];
            }
        }
    }

    public static void main(String[] args) {
        matVector matV = new matVector();
        matV.SetMatrixAndVector();
        long iniCronom = System.currentTimeMillis();
        matV.CalculateProduct();
        long finCronom = System.currentTimeMillis();
        System.out.println("Ha tardado: " + (finCronom - iniCronom));
    }
}