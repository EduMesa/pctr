/**
 * Esta clase realiza el producto interno de dos vectores
 * de manera secuencial
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */
public class prodEscalar
{
    //Vectores sobre los que se realizan las operaciones
    private double[] v1 = new double[1000000];
    private double[] v2 = new double[1000000];
    
    /**
     * Método para asignarle valores a los elementos de cada vector
     */
    public void InicializarVectores()
    {
        for(int i = 0; i < v1.length; i++)
        {
            v1[i] = i + 1;
            v2[i] = i + 1;
        }
    }

    /**
     * Método donde se realiza el cálculo del producto interno
     * @return cálculo realizado
     */
    public double CalcularProdEscalar()
    {
        double res = 0;
        for(int i = 0; i < v1.length; i ++)
        {
            res += v1[i] * v2[i];
        }
        return res;
    }

    public static void main (String[] args)
    {
        prodEscalar prodE = new prodEscalar();
        prodE.InicializarVectores();

        long iniCronom = System.currentTimeMillis();
        System.out.println("El producto escalar es: " + prodE.CalcularProdEscalar());
        long finCronom = System.currentTimeMillis();

        System.out.println("Tiempo tardado: " + (finCronom - iniCronom));
    }
}