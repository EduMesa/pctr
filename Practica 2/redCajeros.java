/**
 * Esta clase hace uso mediante hilos de los carejos 
 * que hemos creado usando para ello un objeto común
 * que representa la cuenta corriente
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */
import java.util.Scanner;

public class redCajeros
{
    public static void main(String[] args) throws InterruptedException
    {
        Scanner s = new Scanner(System.in);
        System.out.println("Introduzca numero de ingresos y reintegros: ");
        int nIter = s.nextInt();

        cuentaCorriente cuentaC = new cuentaCorriente();
        Runnable r1 = new cajero(cuentaC, true, nIter);
        Runnable r2 = new cajero(cuentaC, false, nIter);

        Thread h1 = new Thread(r1);
        Thread h2 = new Thread(r2);

        h1.start();
        h2.start();
        h1.join();
        h2.join();

        System.out.println("El saldo de la cuenta es: " + cuentaC.GetSaldo());
    }
}