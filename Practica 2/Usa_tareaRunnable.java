/**
 * Esta clase hace uso de tareaRunnable. En ella creamos
 * los hilos y el objeto común compartido por ambos.
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */
import java.util.Scanner;

public class Usa_tareaRunnable
{
    public static void main (String[] args) throws InterruptedException
    {
        int nIt = 0;
        Scanner s = new Scanner(System.in);
        System.out.println("Introduzca numero de iteraciones");
        nIt = s.nextInt();
        Critica c = new Critica(); //Referencia al objeto común
        Runnable increment = new tareaRunnable(c, nIt, true);
        Runnable decrement = new tareaRunnable(c, nIt, false);

        Thread incrementThread = new Thread(increment);
        Thread decrementThread = new Thread(decrement);

        incrementThread.start();
        decrementThread.start();
        incrementThread.join();
        decrementThread.join();

        System.out.println("Valor de la variable: " + c.GetN());

    }
}