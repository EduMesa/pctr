/**
 * Esta clase permite incrementar o decrementar la variable de un
 * objeto a través de hilos
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */
public class tareaRunnable implements Runnable {

    /**
     * Constructor de la clase
     * @param c Objeto de la clase Critica
     * @param nIter número de veces a incrementar o decrementar
     * @param increase booleano para saber si se quiere incrementar o decrementar
     */
    public tareaRunnable(Critica c, int nIter, boolean increase)
    {
        this.c = c;
        this.nIter = nIter;
        this.increase = increase;
    }

    /**
     * Sobrecarga del método run. Este método permite incrementar
     * o decrementar el valor del objeto dependiendo del modo elegido
     */
    public void run() {
        if (increase) {
            for (int i = 0; i < nIter; i++) {
                c.Inc();
            }
        } else {
            for (int i = 0; i < nIter; i++) {
                c.Dec();
            }
        }
    }

    //Objeto de la clase Critica
    private Critica c;
    
    //Número de iteraciones a realizar
    public int nIter = 0;

    //Variable para indicar si queremos incrementar o decrementar
    public boolean increase = false;
}