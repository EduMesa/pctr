/**
 * Clase para modelar la cuenta corriente.
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

public class cuentaCorriente
{
    //Saldo en la cuenta
    public int saldo = 0;

    /**
     * Método para realizar un ingreso en la cuenta
     */
    public void Ingreso()
    {
        saldo += 50;
    }

    /**
     * Método para realizar un reintegro en la cuenta
     */
    public void Reintegro()
    {
        saldo -= 50;
    }

    /**
     * Método observador para consultar el saldo
     * @return saldo
     */
    public int GetSaldo()
    {
        return saldo;
    }
}