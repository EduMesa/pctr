/**
 * Esta clase permite incrementar o decrementar 
 * una variable común entre los hilos
 * un número de veces dado
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

public class hebra extends Thread {

    /**
     * Constructor de la clase
     * @param nIter número de veces a incrementar o decrementar
     * @param increase variable que indica si queremos incrementar o decrementar
     */
    public hebra(int nIter, boolean increase) {
        this.nIter = nIter;
        this.increase = increase;
    }

    /**
     * Método observador para consultar el valor de la variable n
     * @return valor de la variable n
     */
    public int GetN() { return n; }

    /**
     * Método run en el que los hilos incrementarán
     * o decrementarán la variable n en función
     * del booleano de la clase
     */
    public void run() {
        if (increase) {
            for (int i = 0; i <= nIter; i++) {
                n++;
            }
        } else {
            for (int i = 0; i <= nIter; i++) {
                n--;
            }
        }
    }

    //Variable compartida por los hilos
    private static int n = 0;

    //Número de iteraciones a realizar
    private int nIter = 0;

    //Variable para indicar si queremos incrementar o decrementar
    private boolean increase;
}