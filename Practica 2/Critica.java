/**
 * Esta clase permite tener un objeto común entre los hilos
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */
public class Critica
{
    //Variable para incrementar o decrementar
    private int n = 0;

    /**
     * Constructor vacío de la clase
     */
    public Critica(){}

    /**
     * Método observador para consultar el valor de n
     * @return la variable n de la case
     */
    public int GetN() { return n; }

    /**
     * Este método permite hacer un incremento sobre la variable n
     */
    public void Inc(){ n++; }

    /**
     * Este método permite hacer un decremento sobre la variable n
     */
    public void Dec(){ n--; }
}