/**
 * Esta clase permite escalar un vector dado un factor de escalado
 * introducido por teclado. Esta clase implementa una solución
 * secuencial
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */
import java.util.Scanner;

public class escalaVector
{
     //Vector a escalar
    private int[] vector = new int[100000000];

    /**
     * Observador del vector
     * @return vector de la clase
     */
    public int[] GetVector() { return vector; }
    
    public static void main (String[] args)
    {
        Scanner s = new Scanner(System.in);
        System.out.println("Introduzca factor de escalado");
        int n = s.nextInt();

        escalaVector escalaV = new escalaVector();

        for(int i = 0; i < escalaV.GetVector().length; i++)
        {
            escalaV.GetVector()[i] = i*n;
        }
    }
}