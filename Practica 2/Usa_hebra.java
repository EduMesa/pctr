/**
 * Esta clase hace uso de la clase hebra. Para ello
 * creamos dos hilos y elegimos el número de iteraciones
 * que van a hacer cada uno de los hlos
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */
import java.util.Scanner;

public class Usa_hebra
{
    public static void main (String[] args) throws InterruptedException
    {
        Scanner s = new Scanner(System.in);
        System.out.println("Introduzca numero de iteraciones");
        int nIter = s.nextInt();
        hebra h1 = new hebra(nIter, true);
        hebra h2 = new hebra(nIter, false);

        h1.start(); //Este método no hace que empiece la hebra, lo que hace es que pueda ser planificada. Cuando el SO lo decida, comenzará su ejecución
        h2.start();

        h1.join(); //Nunca usaremos el join con parametros para finalización.
        h2.join();

        //Definición de co-rutina: Situación donde el programa principal inicia varias tareas de manera concurrente. La característica principal es que
        //el programa principal espera a que las tareas terminen para continuar su ejecución.

        System.out.println("Valor de n: " + h1.GetN());
    }
}