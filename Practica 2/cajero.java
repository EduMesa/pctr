/**
 * Esta clase permite hacer incrementos o decrementos en un objeto
 * del tipo cuentaCorriente a través de hilos
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

public class cajero implements Runnable {

    //Objeto de cuentaCorriente sobre el que se harán los ingresos o los reintegros
    private cuentaCorriente c;

    //Esta variable permite decidir si se quiere hacer un ingreso o un reintegro
    private boolean ingreso = false;

    //Número de iteraciones a realizar
    private int nIter = 0;

    /**
     * Constructor de la clase
     * @param cajero objeto de cuentaCorriente
     * @param ingreso booleano para decidir si hacer reintegro o ingreso
     * @param nIter número de iteraciones a realizar
     */
    public cajero(cuentaCorriente cajero, boolean ingreso, int nIter) {
        c = cajero;
        this.ingreso = ingreso;
        this.nIter = nIter;
    }

    /**
     * Sobrecarga del método run. En él se realizan los ingresos o reintegros
     * un número de veces igual al número de iteraciones introducido.
     * Hacer un ingreso o un reintegro dependerá del booleano
     */
    public void run() {
        if (!ingreso) {
            for (int i = 0; i < nIter; i++) {
                c.Reintegro();
            }
        } else {
            for (int i = 0; i < nIter; i++) {
                c.Ingreso();
            }
        }
    }
}