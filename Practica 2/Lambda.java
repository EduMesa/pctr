/**
 * Esta clase crea una condición de concurso sobre una variable compartida
 * usando para ello expresiones lambda
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */
import java.util.Scanner;

public class Lambda
{
    //Variable compartida
    private static int n = 0;

    /**
     * Método observador para obtener el valor de la variable compartida
     * @return la variable compartida
     */
    public static int GetN() { return n; }

    public static void main (String[] args) throws InterruptedException
    {
        Scanner s = new Scanner(System.in);
        System.out.println("Introduzca número de iteraciones");
        int nIter = s.nextInt();
        
        Runnable r1 = () -> {
            for(int i = 0; i <= nIter; i++)
            {
                n++;
            }
        };

        Runnable r2 = () ->
        {
            for(int i = 0; i <= nIter; i++)
            {
                n--;
            }
        };

        Thread h1 = new Thread (r1);
        Thread h2 = new Thread (r2);
        h1.start();
        h2.start();
        h1.join();
        h2.join();

        System.out.println(GetN());
    }
}