/**
 * Esta clase permite escalar un vector dado un factor de escalado
 * introducido por teclado. La operación de escalar el vector se hace
 * a través sde dos hilos.
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */
import java.util.Scanner;

public class escalaVPar implements Runnable
{
    //Posición inicial y final donde empezará y terminará cada hebra
    private int ini, fin;

    //Factor de escalado
    private static int factor;
    
    //Vector a escalar
    private static int[] vector = new int[100000000];

    //Constructor vacío
    public escalaVPar(){}

    /**
     * Constructor para la clase 
     * @param ini Posición inicial donde empezará la hebra
     * @param fin Posición final donde terminará la hebra
     * @param factor Factor por el cual se va a multiplicar cada elemento del vector
     */
    public escalaVPar(int ini, int fin, int factor)
    {
        this.ini = ini;
        this.fin = fin;
        SetFactor(factor);
        InitializeVector();
    }

    /**
     * Observador para el vector
     * @return Devuelve el vector sobre el que se aplica el escalado
     */
    public int[] GetVector() { return vector; }

    /**
     * Modificador de la variable factor
     * @param f factor de escalado
     */
    public void SetFactor(int f)
    {
        factor = f;
    }
 
    /**
     * Método para rellenar el vector.
     */
    public void InitializeVector() {
        
        for(int i = 0; i < vector.length; i++)
        {
            vector[i] = i + 1;
        }
    }

    /**
     * Sobrecarga del método run para que cada hilo 
     * multiplique cada posición del vector donde 
     * se mueve por el factor dado
     */
    public void run() {
        for(int i = ini; i < fin; i++)
        {
            vector[i] *= factor;
        }        
    }
    
    public static void main (String[] args) throws InterruptedException
    {
        escalaVPar escalaPar = new escalaVPar();
        Scanner s = new Scanner(System.in);
        System.out.println("Introduzca el factor de escalado");
        int factor = s.nextInt();
        Runnable r1 = new escalaVPar(0, escalaPar.GetVector().length / 2, factor);
        Runnable r2 = new escalaVPar((escalaPar.GetVector().length / 2), escalaPar.GetVector().length, factor);

        Thread h1 = new Thread(r1);
        Thread h2 = new Thread(r2);

        h1.start();
        h2.start();

        h1.join();
        h2.join();
    }
}