public class CuerpoPlanetario extends CuerpoAstrofisico
{
    int periodo_orbital;

    CuerpoPlanetario(){}
    CuerpoPlanetario(int periodo)
    {
        periodo_orbital = periodo;
    }

    public int GetOrbitalPeriod() { return periodo_orbital; }
    public void SetOrbitalPeriod(int p) { periodo_orbital = p; }
}