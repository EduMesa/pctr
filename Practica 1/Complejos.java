import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Collections;

public class Complejos {
    private int[] number = new int[2];
    public int[] addition = new int[2];;
    public int[] substract = new int[2];
    public int[] product = new int[2];
    public double[] division = new double[2];
    public double modulus = 0;

    public void SetValues() {
        System.out.println("Introduzca un valor para la parte real y otro para la parte imaginaria");
        Scanner s = new Scanner(System.in);
        for (int i = 0; i < number.length; i++) {
            if (s.hasNextInt()) {
                number[i] = s.nextInt();
            } else {
                System.out.println("Error, debe introducir un número, reiniciando proceso...");
                number[0] = 0;
                number[1] = 0;
                SetValues();
                break;
            }
        }
    }

    public void CalculateAddition(Complejos a)
    {
        addition[0] = this.number[0] + a.number[0];
        addition[1] = this.number[1] + a.number[1];
    }

    public void CalculateSubstract(Complejos a)
    {
        substract[0] = this.number[0] - a.number[0];
        substract[1] = this.number[1] - a.number[1];
    }

    public void CalculateProduct(Complejos a)
    {
        product[0] = this.number[0] * a.number[0] - this.number[1] * a.number[1];
        product[1] = this.number[0] * a.number[1] + this.number[1] * a.number[0];
    }

    public void CalculateDivision(Complejos a)
    {
        division[0] = this.number[0] * a.number[0] + this.number[1] * a.number[1] / Math.pow(a.number[0], 2) + Math.pow(a.number[1], 2);
        division[1] = this.number[1] * a.number[0] - this.number[0] * a.number[1] / Math.pow(a.number[0], 2) + Math.pow(a.number[1], 2);
    }

    public void CalculateModulus()
    {
        modulus = Math.sqrt(Math.pow(this.number[0], 2) + Math.pow(this.number[1], 2));
    }
}