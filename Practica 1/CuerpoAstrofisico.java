public class CuerpoAstrofisico
{
    String nombre;
    double masa;
    double gravedad;
    double temperatura;

    CuerpoAstrofisico(){}
    CuerpoAstrofisico (String nombre, double masa, double gravedad, double temperatura)
    {
        this.nombre = nombre;
        this.masa = masa;
        this.gravedad = gravedad;
        this.temperatura = temperatura;
    }

    public String GetName() { return nombre; }
    public double GetMass() { return masa; }
    public double GetGravity() { return gravedad; }
    public double GetTemperature() { return temperatura; }

    public void SetName(String nombre) { this.nombre = nombre; }
    public void SetMass(double masa) { this.masa = masa; }
    public void SetGravity(double gravedad) { this.gravedad = gravedad; }
    public void SetTemperature(double temperatura) { this.temperatura = temperatura; }
}