import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Collections;

public class Estadistica {
    private ArrayList<Integer> values;
    private int optionSelected;
    private boolean printed = false;
    public int mode = 0;
    public double mean = 0;
    public double variance = 0;
    public double standardDeviation = 0;

    public void SetValues() {
        values = new ArrayList<Integer>();
        System.out.println("Introduzca valores enteros para el cálculo. (Introduzca cualquier caracter para parar)");
        Scanner s = new Scanner(System.in);
        while (s.hasNextInt()) {
            values.add(s.nextInt());
        }
    }

    public boolean InvokeMenu() {
        // Menu
        if (!printed) {
            System.out.println("Opciones: ");
            System.out.println("1.- Media");
            System.out.println("2.- Moda");
            System.out.println("3.- Varianza");
            System.out.println("4.- Desviacion tipica");
            System.out.println("Introduzca una opcion del menu");
            printed = true;
        }
        Scanner s = new Scanner(System.in);
        optionSelected = s.nextInt();

        if (optionSelected > 0 && optionSelected < 5) {
            return true;
        } else {
            System.out.println("Opcion incorrecta!");
            return false;
        }
    }

    public void CalculateMean() {
        for (Integer v : values) {
            mean += v;
        }
        mean = mean / values.size();
    }

    public void CalculateMode() {

        int auxOcurrences, ocurrences = 0;
        for (int i = 0; i < values.size(); i++) {
            auxOcurrences = 1;
            for (int j = i + 1; j < values.size(); j++) {
                if (values.get(i) == values.get(j)) {
                    auxOcurrences++;
                }
            }
            if (auxOcurrences > ocurrences) {
                mode = values.get(i);
                ocurrences = auxOcurrences;
            }
        }

        System.out.println("La moda es: " + mode + " se repite: " + ocurrences + " veces");
    }

    public void CalculateVariance() {

        CalculateMean();
        for (Integer v : values) {
            variance += Math.pow(v - mean, 2);
        }
        variance = variance / values.size();
    }
    
    public void CalculateStandardDeviation()
    {
        CalculateVariance();
        standardDeviation = Math.sqrt(variance);
    }
    public static void main(String[] args) {
        // Creamos un objeto de la clase para poder acceder a los métodos
        Estadistica st = new Estadistica();

        // Rellenamos la lista de enteros
        st.SetValues();

        // Forzamos al usuario a escoger una opción correcta del menú para continuar.
        while (!st.InvokeMenu())
            ;

        // En función de la opción escogida se hace una cosa u otra
        switch (st.optionSelected) {
        case 1:
            st.CalculateMean();
            System.out.println("La media es: " + st.mean);
            break;
        case 2:
            st.CalculateMode();
            break;
        case 3:
            st.CalculateVariance();
            System.out.println("La varianza es: " + st.variance);
            break;

        case 4:
            st.CalculateStandardDeviation();
            System.out.println("La desviacion tipica es : " + st.standardDeviation);
            break;
        }
    }
}