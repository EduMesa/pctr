public class Tierra extends CuerpoPlanetario
{
    boolean existeVida;

    Tierra(){}
    Tierra(boolean vida)
    {
        existeVida = vida;
    }

    public boolean GetLife() { return existeVida; }
    public void SetLife(boolean existeVida) { this.existeVida = existeVida; }
}