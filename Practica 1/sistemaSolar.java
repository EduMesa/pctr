
public class sistemaSolar {
    public static void main(String[] args) {
        CuerpoPlanetario tierra = new CuerpoPlanetario();
        Sol sol = new Sol(new String[] { "Mercurio", "Venus", "Tierra", "Marte", "Jupiter", "Saturano", "Urano",
                "Neptuno", "Pluton" });
        Luna luna = new Luna(1737.1);

        // Luna
        luna.SetAstro("Tierra");
        luna.SetGravity(1.62);
        luna.SetMass(7.349 * Math.pow(10, 22));
        luna.SetName("Luna");
        luna.SetTemperature(123);

        // Sol
        sol.SetDiameter(1.391 * Math.pow(10, 6));
        sol.SetName("Sol");
        sol.SetGravity(274);
        sol.SetMass(1.989 * Math.pow(10, 30));
        sol.SetTemperature(5505);

        // Tierra
        tierra.SetName("Tierra");
        tierra.SetGravity(9.8);
        tierra.SetMass(5.972 * Math.pow(10, 24));
        tierra.SetTemperature(14);
        tierra.SetOrbitalPeriod(365);

        System.out.println("Masa de la " + luna.GetName() + " es: " + luna.GetMass() + " kg, temperatura: "
                + luna.GetTemperature() + " grados centigrados, gravedad: " + luna.GetGravity() + "m/s², radio: "
                + luna.GetRadius() + "km, gira sobre: " + luna.GetAstro());

        System.out.print("Masa del " + sol.GetName() + " es: " + sol.GetMass() + " kg, temperatura: "
                + sol.GetTemperature() + " grados centigrados, gravedad: " + sol.GetGravity() + "m/s², diametro: "
                + sol.GetDiameter() + "km, numero de planetas que orbitan alrededor: " + sol.GetPlanets().length);
        System.out.print(" y son: ");

        String[] planets = sol.GetPlanets();
        for (int i = 0; i < planets.length; i++) {
            if (i == planets.length - 1) {
                System.out.println(planets[i]);
            } else {
                System.out.print(planets[i] + ", ");
            }
        }
        System.out.println("Masa de la " + tierra.GetName() + " es: " + tierra.GetMass() + " kg, temperatura: "
                + tierra.GetTemperature() + " grados centigrados, gravedad: " + tierra.GetGravity()
                + "m/s², periodo orbital: " + tierra.GetOrbitalPeriod() + " dias");

    }
}