import java.util.Scanner;
import java.util.*;
import java.lang.Math;

public class NewtonRaphson {
  public static void main(String[] args) {
    double xN1 = 0, xN2 = 0;

    System.out.println("Introduzca el numero de iteraciones");
    Scanner s = new Scanner(System.in);
    int nIter = s.nextInt();

    System.out.println("Introduzca la aproximacion inicial");
    double aproximacion = s.nextDouble();
    double xN = aproximacion;

    for (int i = 0; i < nIter; i++) {
      xN1 = xN - ((Math.cos(xN) - Math.pow(xN, 3)) / (-Math.sin(xN) - 3 * Math.pow(xN, 2)));
      System.out.println("Aproximacion f(x) = cos(x) - x³ ---> " + xN1);
      xN = xN1;
    }

    for (int i = 0; i < nIter; i++) {
      xN2 = aproximacion - ((Math.pow(aproximacion, 2) - 5) / (2 * aproximacion));
      System.out.println("Aproximacion x² - 5 ---> " + xN2);
      aproximacion = xN2;
    }
  }
}