public class Satelite extends CuerpoAstrofisico
{
    String astroSobreQuienGiro;
    
    Satelite(){}
    Satelite(String astro) {
        astroSobreQuienGiro = astro;
    }

    public String GetAstro() { return astroSobreQuienGiro; }
    public void SetAstro(String astro) { astroSobreQuienGiro = astro; }
}