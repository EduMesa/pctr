public class Estrella extends CuerpoAstrofisico
{
    double diametro;

    Estrella(){}
    Estrella(double diametro){
        this.diametro = diametro;
    }

    public double GetDiameter() { return diametro; }
    public void SetDiameter(double diametro) { this.diametro = diametro; }
}