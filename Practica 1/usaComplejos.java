import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Collections;

public class usaComplejos {
    private int optionSelected;
    private boolean printed = false;

    public boolean InvokeMenu() {
        // Menu
        if (!printed) {
            System.out.println("Opciones: ");
            System.out.println("1.- Suma");
            System.out.println("2.- Resta");
            System.out.println("3.- Modulo");
            System.out.println("4.- Producto");
            System.out.println("5.- Cociente");
            System.out.println("Introduzca una opcion del menu");
            printed = true;
        }
        Scanner s = new Scanner(System.in);
        optionSelected = s.nextInt();

        if (optionSelected > 0 && optionSelected < 6) {
            return true;
        } else {
            System.out.println("Opcion incorrecta!");
            return false;
        }
    }

    public static void main(String[] args) {
        // Creamos un objeto de la clase para poder acceder a los métodos
        Complejos cmp = new Complejos();

        // Rellenamos el complejo
        cmp.SetValues();

        Complejos cmp1 = new Complejos();
        cmp1.SetValues();

        usaComplejos usaComplejos = new usaComplejos();

        // Forzamos al usuario a escoger una opción correcta del menú para continuar.
        while (!usaComplejos.InvokeMenu()) {
        }

        // En función de la opción escogida se hace una cosa u otra
        switch (usaComplejos.optionSelected) {
        case 1:
            cmp.CalculateAddition(cmp1);
            if (cmp.addition[1] >= 0) {
                System.out.println("El resultado de la suma de ambos compejos es: " + cmp.addition[0] + "+"
                        + cmp.addition[1] + "i");
            } else {
                System.out.println(
                        "El resultado de la suma de ambos compejos es: " + cmp.addition[0] + cmp.addition[1] + "i");
            }
            break;
        case 2:
            cmp.CalculateSubstract(cmp1);
            if (cmp.substract[1] >= 0) {
                System.out.println("El resultado de la resta de ambos compejos es: " + cmp.substract[0] + "+"
                        + cmp.substract[1] + "i");
            } else {
                System.out.println(
                        "El resultado de la resta de ambos compejos es: " + cmp.substract[0] + cmp.substract[1] + "i");
            }
            break;
        case 3:
            cmp.CalculateModulus();
            System.out.println("El modulo del primer complejo es: " + cmp.modulus);
            cmp1.CalculateModulus();
            System.out.println("El modulo del segundo complejo es: " + cmp1.modulus);
            break;

        case 4:
            cmp.CalculateProduct(cmp1);
            if (cmp.product[1] >= 0) {
                System.out.println("El resultado de la multiplicacion de ambos compejos es: " + cmp.product[0] + "+"
                        + cmp.product[1] + "i");
            } else {
                System.out.println("El resultado de la multiplicacion de ambos compejos es: " + cmp.product[0]
                        + cmp.product[1] + "i");
            }
            break;
        case 5:
            cmp.CalculateDivision(cmp1);
            if (cmp.division[1] >= 0) {
                System.out.println("El resultado de la division de ambos compejos es: " + cmp.division[0] + "+"
                        + cmp.division[1] + "i");
            } else {
                System.out.println(
                        "El resultado de la division de ambos compejos es: " + cmp.division[0] + cmp.division[1] + "i");
            }
            break;
        }
    }
}