import java.util.Scanner;
import java.util.Random;

public class intDefinidaMonteCarlo {
  public static void main(String[] args) {

    System.out.println("Introduzca el numero de intentos");
    Scanner s = new Scanner(System.in);
    int n = s.nextInt();
    int exitosX = 0, exitosSin = 0;
    double coordenada_x = 0;
    double coordenada_y = 0;
    Random r = new Random();

    for (int i = 0; i < n; i++) {

      coordenada_x = r.nextDouble();
      coordenada_y = r.nextDouble();

      if(coordenada_y < Math.sin(coordenada_x))
      {
        exitosSin++;
      }
      if (coordenada_y < coordenada_x) {
        exitosX++;
      }
    }
    System.out.println("Aproximacion f(x) = sin --->" + ((double) exitosSin / (double) n));
    System.out.println("Aproximacion f(x) = x: --->" + ((double) exitosX / (double) n));
  }
}