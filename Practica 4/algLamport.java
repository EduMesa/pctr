
/**
 * Esta clase implementa el algoritmo de Lamport a partir
 * del código adaptado de M.Ben-Ari por Manuel Francisco.
 * La clase está hecha para N procesos a través de la 
 * implementación de la interfaz Runnable
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class algLamport implements Runnable {

    // array de enteros que notifica la intención de entrar en SC
    public static int[] h;

    // Recurso compartido que se incrementará por cada hebra
    // Iteraciones que realizará cada hebra
    public static int recursoCompartido = 0, iteraciones;

    // Indice que representa la posición de cada hebra
    private int indice;

    /**
     * Constructor de la clase
     * 
     * @param indice
     */
    public algLamport(int indice) {
        this.indice = indice;
    }

    /**
     * Método observador para consultar el recurso compartido
     * 
     * @return
     */
    public static int GetResource() {
        return recursoCompartido;
    }

    /**
     * Sobrecarga del método run
     */
    public void run() {
        for (int i = 0; i < iteraciones; i++) {
            boolean flag = false;
            h[indice]++;
            while (!flag) {
                for (int j = 0; j < h.length; j++) {
                    if ((h[j] == 0 || h[indice] <= h[j])) {
                        flag = true;
                    }
                }
                Thread.yield();
            }

            recursoCompartido++;

            h[indice] = 0;
        }
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Introduzca numero de hilos: ");
        h = new int[s.nextInt()];
        System.out.print("Introduzca numero de iteraciones: ");
        iteraciones = s.nextInt();

        ExecutorService threadPool = Executors.newFixedThreadPool(h.length);

        for (int i = 0; i < h.length; i++) {
            threadPool.execute(new algLamport(i));
        }
        threadPool.shutdown();
        try {
            threadPool.awaitTermination(20L, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
        }

        System.out.println("El valor del recurso compartido es: " + GetResource());
    }
}
