
/**
 * Esta clase implementa el algoritmo de EisenbergMcGuire
 * para dos hilos usando un ejecutor de tamaño fijo
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class algEisenbergMcGuire implements Runnable {
    
    //Variable que representa el numero de procesos
    final static int nProcesos = 2;

    //Id de cada hebra
    private int idHebra;

    //Iteraciones a realizar
    //Turno de cada hebra
    //Recurso compartido a incrementar por cada hebra
    static int iteraciones, turno, recursoCompartido = 0;
    
    //Booleano para inicializar ciertos parametros una sola vez
    static boolean once = false;
    
    //Array de estados (enum) que representa el estado de cada hebra
    static estados[] bandera;

    //Enumerado de los posibles estados de una hebra
    enum estados {
        LIBRE, ESPERANDO, ACTIVO
    };

    /**
     * Constructor de la clase
     * @param idHebra id de la hebra
     * @param nIter numero de iteraciones a realizar
     */
    public algEisenbergMcGuire(int idHebra, int nIter) {
        this.idHebra = idHebra;
        if (!once) {
            iteraciones = nIter;
            bandera = new estados[nProcesos];
            SetTurno(0);
            InicializarEstados();
            once = true;
        }
    }

    /**
     * Método modificador del turno
     * @param t turno
     */
    public void SetTurno(int t) {
        turno = t;
    }

    /**
     * Método observador para obtener el valor del recurso compartido
     * @return
     */
    public int GetResource() {
        return recursoCompartido;
    }

    /**
     * Método para inicializar los valores de los estados
     */
    public void InicializarEstados() {
        for (int i = 0; i < bandera.length; i++) {
            bandera[i] = estados.LIBRE;
        }
    }

    /**
     * Método que hace el pre protocolo de entrada
     * a la seccion critica. 
     * @param i id de la hebra
     */
    void ProtocoloEntrada(int i) {
        int indice;
        do {
            bandera[i] = estados.ESPERANDO;

            indice = turno;

            while (i != indice) {
                if (bandera[indice] != estados.LIBRE) {
                    indice = turno;
                } else {
                    indice = (indice + 1) % nProcesos;
                }
            }

            bandera[i] = estados.ACTIVO;
            indice = 0;

            while ((indice < nProcesos) && ((indice == i) || (bandera[indice] != estados.ACTIVO)))
                indice++;
        } while (!((indice >= nProcesos) && ((turno == i) || (bandera[turno] == estados.LIBRE))));

        turno = i;
    }

    /**
     * Metodo que realiza el post protocolo de salida
     * de la seccion critica
     * @param i
     */
    void ProtocoloSalida(int i) {
        int indice = (turno + 1) % nProcesos;
        while (bandera[indice] == estados.LIBRE) {
            indice = (indice + 1) % nProcesos;
        }

        turno = indice;
        bandera[i] = estados.LIBRE;
    }

    /**
     * Sobrecarga del metodo run
     */
    public void run() {

        ProtocoloEntrada(idHebra);
        for (int i = 0; i < iteraciones; i++) {
            recursoCompartido++;
        }
        ProtocoloSalida(idHebra);
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Introduzca numero de incrementos a realizar: ");
        int nIter = s.nextInt();
        algEisenbergMcGuire[] algEisenbergMcGuires = new algEisenbergMcGuire[nProcesos];

        ExecutorService threadPool = Executors.newFixedThreadPool(nProcesos);

        for (int i = 0; i < algEisenbergMcGuires.length; i++) {
            threadPool.execute(new algEisenbergMcGuire(i, nIter));
        }

        threadPool.shutdown();
        try {
            threadPool.awaitTermination(5L, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
        }

        System.out.println("Valor del recurso compartido: " + recursoCompartido);
    }
}