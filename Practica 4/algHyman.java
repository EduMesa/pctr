
/**
 * Esta clase implementa el algoritmo de Hyman haciendo
 * uso de expresiones lambda para ello
 * @author Eduardo Mesa Orcero
 * @version 0.1
 */

import java.util.Scanner;

public class algHyman {

    //Turno representa el turno de cada hebra
    //RecursoCompartido representa el entero que 
    //incrementará cada hebra
    static int turno = 0, recursoCompartido = 0;
    
    //Bandera de cada hebra que representa su intención
    //de entrar en la seccion critica
    static boolean[] banderas;

    /**
     * Pequeño método para inicializar el array de booleanos
     * @param tam tamaño al que inicializar el array
     */
    public static void InicalizarBanderas(int tam) {
        banderas = new boolean[tam];
        for (int i = 0; i < tam; i++) {
            banderas[i] = false;
        }
    }

    /**
     * Método observador para obtener el valor del recurso compartido
     * @return el recurso compartido
     */
    public static int GetRecursoCompartido() {
        return recursoCompartido;
    }

    public static void main(String[] args) throws InterruptedException {
        Scanner s = new Scanner(System.in);
        System.out.print("Introduzca numero de hebras: ");
        int nHebras = s.nextInt();
        System.out.print("Introduzca numero de incrementos a realizar por cada hebra: ");
        int iteraciones = s.nextInt();
        InicalizarBanderas(nHebras);
        Thread[] hilos = new Thread[nHebras];

        for (int i = 0; i < nHebras; i++) {
            final int idHebra = i;

            hilos[i] = new Thread(() -> {

                banderas[idHebra] = true;

                while (turno != idHebra) {
                    while (banderas[(idHebra + 1) % nHebras])
                        ;

                    turno = idHebra;
                }

                for (int j = 0; j < iteraciones; j++)
                    recursoCompartido++;

                banderas[idHebra] = false;

            });
            hilos[i].start();
        }

        for (int i = 0; i < nHebras; i++) {
            hilos[i].join();
        }

        System.out.println("Valor final esperado: " + (nHebras * iteraciones));
        System.out.println("Valor final obtenido: " + GetRecursoCompartido());

    }
}